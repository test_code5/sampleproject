package fr.teknoup.sampleproject.services.impl;

import fr.teknoup.sampleproject.models.entities.RealStateAd;
import fr.teknoup.sampleproject.models.repositories.RealStateAdRepository;
import fr.teknoup.sampleproject.services.RealStateAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Service
public class RealStateAdServiceImp implements RealStateAdService {

    private static Logger logger = Logger.getLogger(String.valueOf(RealStateAdService.class));

    @Autowired
    private RealStateAdRepository realStateAdRepository;

    @Override
    public RealStateAd save(RealStateAd announce) {
        announce.setCreatedAt(new Date());

        return realStateAdRepository.save(announce);
    }

    @Override
    public RealStateAd update(RealStateAd announce, Integer id) {
        announce.setUpdateAt(new Date());
        RealStateAd old =  realStateAdRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Announce not found for this id :: " + id));

        if (old!=null){
            throw new ResourceNotFoundException("Announce  id" +id);
        }else{
            if(announce.getTitle()!= null && !announce.getTitle().isBlank()) {
                old.setTitle(old.getTitle());
            }
            if(announce.getPlace()!= null && !announce.getPlace().isBlank()) {
                old.setTitle(old.getPlace());
            }
            return realStateAdRepository.saveAndFlush(announce);

        }
    }

    @Override
    public RealStateAd getOne(Integer id) {
        RealStateAd old =  realStateAdRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Announce not found for this id :: " + id));
        return old;
    }

    @Override
    public RealStateAd findByTitle(String title) {
        return realStateAdRepository.findByTitleLike(title);
    }

    @Override
    public RealStateAd findByPlace(String place) {
        return realStateAdRepository.findByPlaceLike(place);
    }

    @Override
    public void delete(Integer id) {
        realStateAdRepository.deleteById(id);
    }

    @Override
    public List<RealStateAd> search(String value) {
        return realStateAdRepository.findByTitleContainingOrPlaceContaining(value, value);
    }

    @Override
    public List<RealStateAd> searchByTitle(String title) {
        return realStateAdRepository.findByTitleContaining(title);
    }

    @Override
    public List<RealStateAd> getAll() {
        List<RealStateAd> list= realStateAdRepository.findAll();

        if (list.isEmpty()){
            throw new ResourceNotFoundException("No data available" );
        }else{
            return list;

        }
    }
}
