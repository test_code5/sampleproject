package fr.teknoup.sampleproject.services;

import fr.teknoup.sampleproject.models.entities.RealStateAd;

import java.util.List;

public interface RealStateAdService {

      RealStateAd save(RealStateAd announce);
      RealStateAd update(RealStateAd announce, Integer id);
      RealStateAd getOne(Integer id);
      RealStateAd findByTitle(String title);
      RealStateAd findByPlace(String place);
      void delete(Integer id);

      List<RealStateAd> search(String value);
      List<RealStateAd> searchByTitle(String title);
      List<RealStateAd> getAll();
}
