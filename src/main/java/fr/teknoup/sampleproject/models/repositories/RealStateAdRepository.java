package fr.teknoup.sampleproject.models.repositories;

import fr.teknoup.sampleproject.models.entities.RealStateAd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RealStateAdRepository extends JpaRepository<RealStateAd, Integer> {
    RealStateAd findByTitle(String title);

    RealStateAd findByTitleLike(String title);

    List<RealStateAd>  findByTitleContaining(String title);

    List<RealStateAd> findByPlaceContaining(String place);
    List<RealStateAd>  findByTitleContainingOrPlaceContaining(String title, String place);
    RealStateAd findByPlaceLike(String place);
}