package fr.teknoup.sampleproject.models.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@ApiModel(value = "RealStateAd Model: La table qui représente  annonces immobilières")
public class RealStateAd implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "RealStateAd id")
    private Integer  id;

    @ApiModelProperty(value = "RealStateAd title")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "RealStateAd description")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "RealStateAd picture")
    @Column(name = "picture")
    private String picture;

    @ApiModelProperty(value = "RealStateAd place")
    @Column(name = "place")
    private String place;

    @ApiModelProperty(value = "RealStateAd createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @ApiModelProperty(value = "RealStateAd updateAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateAt;

    public RealStateAd() {
    }

    public RealStateAd(Integer id, String title, String description, String picture, String place, Date createdAt, Date updateAt) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.picture = picture;
        this.place = place;
        this.createdAt = createdAt;
        this.updateAt = updateAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }


    @ApiModelProperty(value = "RealStateAd picturePath")
    @Transient
    public String getPicturePath() {
        if (picture == null || id == null) return null;

        return "/images/" + id + "/" + picture;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RealStateAd)) return false;
        RealStateAd that = (RealStateAd) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "RealStateAd{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", picture='" + picture + '\'' +
                ", createdAt=" + createdAt +
                ", updateAt=" + updateAt +
                '}';
    }
}
