package fr.teknoup.sampleproject.controllers;

import fr.teknoup.sampleproject.config.FileUploadUtil;
import fr.teknoup.sampleproject.models.entities.RealStateAd;
import fr.teknoup.sampleproject.services.impl.RealStateAdServiceImp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/announcements")
@Api("API pour  RealStateAdController.")
public class RealStateAdController {

    @Autowired
    private RealStateAdServiceImp realStateAdService;

    @CrossOrigin
    @ApiOperation(value = "Ajouter une annonce!")
    @PostMapping("/")
    public ResponseEntity<RealStateAd> save(RealStateAd announce,   @RequestParam("image") MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        announce.setPicture(fileName);

        RealStateAd saved  = realStateAdService.save(announce);

        String uploadDir = "images/" + saved.getId();

         FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }
    @CrossOrigin
    @ApiOperation(value = "Modifier une annonce!")
    @PutMapping("/{id}")
    public ResponseEntity<RealStateAd> update(  @RequestParam("image") MultipartFile multipartFile, @RequestBody RealStateAd announce, @PathVariable(value = "id") Integer id) throws IOException {
        RealStateAd announceOld=realStateAdService.getOne(id);
        if (multipartFile != null) {
            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            announce.setPicture(fileName);
            RealStateAd announceUpdated= realStateAdService.update(announce, id);
            String uploadDir = "images/" + announceUpdated.getId();

            FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
            return new ResponseEntity<>(announceUpdated, HttpStatus.OK);

        }else{

            RealStateAd announceUpdated= realStateAdService.update(announce, id);

            return new ResponseEntity<>(announceUpdated, HttpStatus.OK);
        }

    }

    @CrossOrigin
    @ApiOperation(value = "Voir une annonce!")
    @GetMapping("/{id}")
    public ResponseEntity<RealStateAd> getOne(@PathVariable Integer id) {
        RealStateAd announce= realStateAdService.getOne(id);
        return new ResponseEntity<>(announce, HttpStatus.FOUND);
    }

    @CrossOrigin
    @ApiOperation(value = "Chercher par titre une annonce!")
    @GetMapping("/findByTitle")
    public ResponseEntity<RealStateAd> findByTitle(@RequestParam String title) {
         RealStateAd announce= realStateAdService.findByTitle(title);
        return new ResponseEntity<>(announce, HttpStatus.FOUND);
    }

    @ApiOperation(value = "Chercher par lieu une annonce!")
    @GetMapping("/searchByPlace")
    public ResponseEntity<RealStateAd> findByPlace(@RequestParam String place) {
        RealStateAd announce = realStateAdService.findByPlace(place);
        return new ResponseEntity<>(announce, HttpStatus.FOUND);
    }

    @CrossOrigin
    @ApiOperation(value = "Supprimer une annonce!")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        realStateAdService.delete(id);
    }

    @CrossOrigin
    @ApiOperation(value = "Chercher une annonce!")
    @GetMapping("/search")
    public ResponseEntity<List<RealStateAd>> search(@RequestParam String value) {
        List<RealStateAd> announces = realStateAdService.search(value);
        return new ResponseEntity<>(announces, HttpStatus.FOUND);
    }

    @CrossOrigin
    @ApiOperation(value = "Chercher par titre une annonce!")
    @GetMapping("/searchByTitle")
    public List<RealStateAd> searchByTitle(@RequestParam(name = "title") String title) {
        return realStateAdService.searchByTitle(title);
    }

    @CrossOrigin
    @ApiOperation(value = "Récupère la liste des annonces !")
    @GetMapping("/")
    public ResponseEntity<List<RealStateAd>> all(){
        List<RealStateAd> announces= realStateAdService.getAll();
        return new ResponseEntity<List<RealStateAd>>(announces, HttpStatus.OK);

    }
}
